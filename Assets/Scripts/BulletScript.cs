﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {

    public GameObject dustParticle;

    private void Update() {
        Destroy(gameObject, 2);
    }

    private void OnTriggerEnter(Collider other) {
        Destroy(gameObject);
        if (other.name.Equals("Terrain")) {
            GameObject go = Instantiate(dustParticle, transform.position, Quaternion.Euler(-90, 0, 0));
            Destroy(go, 1);
        }
    }
}

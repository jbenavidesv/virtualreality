﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cubiquity;

public class MinigunScript : MonoBehaviour {

    private GameObject body;
    private GameObject cannon;
    public GameObject bullet;
    private float isFiring;
    private Vector3 initialPosition;
    private Vector3 cannonInitialPosition;

    void Start () {
        body = transform.GetChild(0).gameObject;
        cannon= transform.GetChild(1).gameObject;
        isFiring = 0;
        initialPosition = body.transform.localPosition;
        cannonInitialPosition = cannon.transform.localPosition;
    }
	
	void Update () {
        if (Input.GetButton("Fire1") && isFiring <= 0) {
            FireBulllet();
        }

        if (isFiring > 0) {
            isFiring -= Time.deltaTime;
            if (isFiring < 0.05f) {
                cannon.transform.localPosition = cannonInitialPosition; // Barril fijo
                body.transform.localPosition = initialPosition;
            }
        }

        transform.localRotation = Quaternion.Euler(
            Input.GetAxis("Vertical") * 60,
            -Input.GetAxis("Horizontal") * 60,
            0
        );

    }

    void FireBulllet() {
        GameObject go = Instantiate(bullet, transform.position, transform.rotation);
        go.GetComponent<Rigidbody>().AddForce(transform.rotation * Vector3.forward * 880);
        isFiring = 1.0f / 8.0f;

        body.transform.localPosition = new Vector3(
            body.transform.localPosition.x,
            body.transform.localPosition.y,
            body.transform.localPosition.z - 0.1f
        ); // Retroceso del barril

        cannon.transform.localPosition = new Vector3(
            cannon.transform.localPosition.x,
            cannon.transform.localPosition.y,
            cannon.transform.localPosition.z - 0.5f
        ); // Retroceso del barril

        Ray ray = new Ray(transform.position, transform.forward);

        GameObject[] terrains = GameObject.FindGameObjectsWithTag("cubiquity");
        foreach (GameObject t in terrains) {
            PickSurfaceResult pickResult;
            bool hit = Picking.PickSurface(t.GetComponent<TerrainVolume>(), ray, 1000.0f, out pickResult);

            if (hit) {
                int range = 2;
                t.GetComponent<DestroyOnHit>().DestroyVoxels((int)pickResult.volumeSpacePos.x, (int)pickResult.volumeSpacePos.y, (int)pickResult.volumeSpacePos.z, range);
            }
        }
    }
}
